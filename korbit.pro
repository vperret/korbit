;+
; NAME:
;	korbit
; PURPOSE:
;	Compute keplerian orbits for galactic interactions
; INPUTS:
;	M1:
;		mass of the first galaxy in 1E10 Solar Mass unit
;	M2:
;		mass of the second galaxy in 1E10 Solar Mass unit
;	SEP:
;		distance between the two galaxies, in kpc. If the kewyord TIMESEP is set,
;		then SEP is the time for the galaxies to reach the periapsis (pericenter) of their trajectory, in Myrs
;	PER
;		pericentral distance, ie. the minimal distance between the two objects with parabolic trajectories
;	E:
;		Eccentricity of the conic section of the trajectories
;
; OPTIONAL OUTPUTS
;	POS_XYZ:
;		a vector of dimension [3,2] containing the positions of the galaxies.
;		Position of the first galaxy:	POS_XYZ[*,0]
;		Position of the second galaxy:	POS_XYZ[*,1]
;	VEL_XYZ
;		a vector of dimension [3,2] containing the velocity vectors of the galaxies
;		Velocity vector of the first galaxy:	VEL_XYZ[*,0]
;		Velocity vector of the second galaxy:	VEL_XYZ[*,1]
;
; OPTIONAL INPUTS:
;	PLOT:
;		Display a window on screen with a plot representing the interaction.
;	BOXLEN:
;		The size of the box for the PLOT option
;	THETA:
;		Rotate the two galaxies in order to ensure that they will be aligned with the diagonal of the box.
;		This allows to use smaller BOXLEN parameter with higher SEP parameter.
;	CUT1:
;		Size of the circle repreenting the galaxy 1 when using PLOT keyword
;	CUT2:
;		Size of the circle repreenting the galaxy 2 when using PLOT keyword
;	VERBOSE:
;		Display informations about the merger parameters on screen instead of writing in a file
;	OUTPUT_NAME:
;		name of the ascii file which will contain the parameters of the orbit. Default value is 'orbit.info'
;	TIMESEP:
;		Allows to set the initial distance between the galaxies using the pericentral time parameter.
;		It means that the initial distance between galaxies is computed to obtain a time
;		to get to the pericenter equal to SEP, expressed in Myrs 
; EXAMPLE:
;	compute_parabolic_merger,10.,20.,50.,10.,1.0,/plot
;
;	This call computes the postion of the barycenter of 2 galaxies
;	with masses 10.E10 and 20.E10 solar masses, separated by 50 kpc and with a pericenter distance of 10 kpc.
;
; MODIFICATION HISTORY:
; 	Written by:	Valentin Perret, October 2012.
;                       e-mail: perret.valentin@gmail.com
;-
pro korbit,m1,m2,sep,per,e,pos_xyz=pos_xyz,vel_xyz=vel_xyz,output_name=output_name,$
	boxlen=boxlen,xrange=xrange,yrange=yrange,plot=plot,theta=theta,cut1=cut1,cut2=cut2,verbose=verbose,center=center,phi=phi,$
	xy=xy,xz=xz,yz=yz,eps=eps,timesep=timesep,specific_orbital_energy=specific_orbital_energy,silent=silent,$
	ratio_xy=ratio_xy,vfactor=vfactor,title=title,thick=thick,x1=x1,y1=y1,z1=z1,x2=x2,y2=y2,z2=z2,no_x_align=no_x_align,nvect=nvect
	
	compile_opt IDL2
	
	if(n_params() ne 5) then begin
		doc_library,'compute_orbit_merger'
		return
	endif
	if(~keyword_set(output_name)) then output_name = 'orbit.info'
	
	if(per gt sep and keyword_set(timesep)) then begin
		print,"The pericentral distance must be smaller than the initial distance between the galaxies"
		return
	endif
	
	if(n_elements(nvect) eq 0) then nvect = 10000
	
	; Constants definition
	; Gravitational constant in meters^3/kilograms/seconds^2 
	G = 6.67428D-11
	;kilo-parsec in meters
	kpc = 3.085678D19
	;1E10 Solar mass in kilograms
	unit_mass = 1.9891D40
	e	= double(e)
	if(e lt 0) then begin
		print,"The eccentricity of the orbits should be greater than 0. Returning."
		return
	endif
	if(keyword_set(timesep)) then timesep = sep else rini = sep
	mtot1 	= double(m1)
	mtot2 	= double(m2)
	
	; Mu is the mass of the reduced particle
	mu = (mtot1*mtot2)/(mtot1+mtot2)
	
	if(n_elements(xrange) eq 0) then xrange = [-sep,sep]
	if(n_elements(yrange) eq 0) then yrange = [-sep,sep]
	if(n_elements(ratio_xy) eq 0) then ratio_xy = 1.0
	; Proportionnality coefficient
	k1 = mu/mtot1
	k2 = mu/mtot2
	; Standard gravitational parameter
	gamma = G*unit_mass*(mtot1+mtot2)
	
	if(n_elements(specific_orbital_energy) ne 0) then begin
		if(keyword_set(timesep)) then begin
			print,'It is not possible tu use the TIMESEP and SPECIFIC_ORBITAL_ENERGY at the same time. Returning.'
		endif
		; Elliptic orbits
		if(specific_orbital_energy lt 0) then $
		e = 1-2.0*per*((1.0/rini)-(kpc*1E-3*1E9/gamma)*(specific_orbital_energy*1.0E4+(G/1.0E9)*unit_mass*(mtot1+mtot2)/((rini)*kpc/1.E3)))
		; Parabolic orbits
		if(specific_orbital_energy eq 0) then $
		e = 1
		; Hyperbolic orbits
		if(specific_orbital_energy gt 0) then $
		e = 1+2.0*per*((1.0/rini)-(kpc*1E-3*1E9/gamma)*(specific_orbital_energy*1.0E4+(G/1.0E9)*unit_mass*(mtot1+mtot2)/((rini)*kpc/1.E3)))
	endif
	
	; Computing parameters of the conic section
	if(e eq 1) then a1 = k1*per else a1 = k1*per/abs(1-e)
	if(e eq 1) then a2 = k2*per else a2 = k2*per/abs(1-e)
	if(e eq 1) then l1 = 2.0*a1 else l1=a1*abs(1-e^2)
	if(e eq 1) then l2 = 2.0*a2 else l2=a2*abs(1-e^2)
	
	; Reduced particle parameters
	a = per/abs(1-e)
	l = per*(1+e)
	; Computing Keplerian trajectories for each galaxy
	nu_vec = (dindgen(nvect)/float(nvect))*2.0*!pi
	; Distance to the barycenter equation
	r1 = l1 /(1+e*cos(nu_vec))
	; Hyperbolic trajectory case
	; Eliminate the imaginary radii
	if(e gt 1.0) then begin
		;ok = where((1+e*cos(nu_vec)) gt 0.0)
		;r1 = r1[ok]
		;nu_vec = nu_vec[ok] 
	endif
	; Velocity in the barycenter frame
	v1 = sqrt((k1*sqrt(gamma/(l*kpc))*sin(nu_vec))^2+(k1*sqrt(gamma/(l*kpc))*(e+cos(nu_vec)))^2)
	; Cartesian x,y,z coordinates
	xt1 = r1*cos(nu_vec)
	yt1 = r1*sin(nu_vec)
	zt1 = 0.*nu_vec
	; Distance to the barycenter equation
	r2 = -l2 /(1+e*cos(nu_vec))
	; Velocity in the barycenter frame
	v2 = sqrt((k2*sqrt(gamma/(l*kpc))*sin(nu_vec))^2+(k2*sqrt(gamma/(l*kpc))*(e+cos(nu_vec)))^2)
	; Cartesian x,y,z coordinates
	xt2 = r2*cos(nu_vec)
	yt2 = r2*sin(nu_vec)
	zt2 = 0.*nu_vec

	t1=fltarr(n_elements(r1))
	t1[0]=0
	t2=fltarr(n_elements(r2))
	t2[0]=0
	deltar1=fltarr(n_elements(r1))
	deltar2=fltarr(n_elements(r2))
	mean_v1=fltarr(n_elements(r1))
	mean_v2=fltarr(n_elements(r2))
	
	for i=0,n_elements(r1)-2 do begin
		; We mesure the distance on the trajectory between r[i] and r[i+1]
		deltar1[i] = sqrt((xt1[i+1]-xt1[i])^2+(yt1[i+1]-yt1[i])^2+(zt1[i+1]-zt1[i])^2)
		deltar2[i] = sqrt((xt2[i+1]-xt2[i])^2+(yt2[i+1]-yt2[i])^2+(zt2[i+1]-zt2[i])^2)
		; We measure the mean velocity in this bin
		mean_v1[i] = mean([v1[i:i+1]])
		mean_v2[i] = mean([v2[i:i+1]])
		; We obtain the timestep over the whole trajectory
		t1[i]=(deltar1[i]*kpc)/(mean_v1[i]*3600*24*365.25*1E6)
		t2[i]=(deltar2[i]*kpc)/(mean_v2[i]*3600*24*365.25*1E6)
	endfor
	if(keyword_set(timesep)) then begin
		rini = per*1.01
		while(1) do begin
			ww1=where(r1 le k1*rini and nu_vec lt !pi)
			t_until_per1=total((t1[ww1]),/nan)
			; If we have reached the required time or the the radius is greater than the apoapis in e<1 cases
			if(t_until_per1 gt timesep) then break
			if((e lt 1.0 and rini gt (1+e)*a)) then begin
				print,"Can't do better than ",t_until_per1
				break
			endif
			rini+=per*0.005
		endwhile
		; We are just interested by the part of the trajectory between the initial position and the pericenter
		ww1=where( r1 lt k1*rini and nu_vec lt !pi)
		ww2=where(-r2 lt k2*rini and nu_vec lt !pi)
		; Time to get to the pericenter for each galaxy
		t_until_per1=total((t1[ww1]),/nan)
		t_until_per2=total((t2[ww2]),/nan)
	endif

	; Radius of galaxies with respect to the barycenter of the system
	radius1 = k1*double(rini)
	radius2 = k2*double(rini)
	; Computing true anomalies
	nu1 = acos(((double(l1)/radius1)-1.0)/e)
	nu2 = acos(((double(l2)/radius2)-1.0)/e)
	; Cases where the initial separation between the two galaxies is too high for the input eccentricity
	; In this case we use the biggest distance allowed by this orbital configuration.
	if(e lt 1.0) then begin
		if(~finite(nu1)) then begin
			nu1=-!pi
			radius1 = l1/(1.-e)
		endif
		if(~finite(nu2)) then begin
			nu2=-!pi
			radius2 = l2/(1.-e)
		endif
	endif
	; Setting cartesian coordinates
	x_1 = radius1*cos(nu1)
	y_1 = radius1*sin(nu1)
	z_1 = 0.
	x_2 = -radius2*cos(nu2)
	y_2 = -radius2*sin(nu2)
	z_2 = 0.
	
	; Setting velocities in cartesian coordinates
	vx_1 =  k1*sqrt(gamma/(l*kpc))*sin(nu1)/1.0E3
	vy_1 = -k1*sqrt(gamma/(l*kpc))*(e+cos(nu1))/1.0E3
	vz_1 =  0.
	vx_2 = -k2*sqrt(gamma/(l*kpc))*sin(nu2)/1.0E3
	vy_2 =  k2*sqrt(gamma/(l*kpc))*(e+cos(nu2))/1.0E3
	vz_2 =  0.
	
	if(~keyword_set(no_x_align)) then begin
		if(x_1 lt 0.) then  theta_gal = asin(y_1/radius1) $
		else theta_gal = asin(-y_1/radius1)
		; Aligning galaxies with X-axis
		x_1_aligned = x_1*cos(theta_gal)-y_1*sin(theta_gal)
		y_1_aligned = x_1*sin(theta_gal)+y_1*cos(theta_gal)
		z_1_aligned = z_1
		x_2_aligned = x_2*cos(theta_gal)-y_2*sin(theta_gal)
		y_2_aligned = x_2*sin(theta_gal)+y_2*cos(theta_gal)
		z_2_aligned = z_2
		vx_1_aligned = vx_1*cos(theta_gal)-vy_1*sin(theta_gal)
		vy_1_aligned = vx_1*sin(theta_gal)+vy_1*cos(theta_gal)
		vz_1_aligned = vz_1
		vx_2_aligned = vx_2*cos(theta_gal)-vy_2*sin(theta_gal)
		vy_2_aligned = vx_2*sin(theta_gal)+vy_2*cos(theta_gal)
		vz_2_aligned = vz_2
	endif else begin
		x_1_aligned = x_1
		y_1_aligned = y_1
		z_1_aligned = z_1
		x_2_aligned = x_2
		y_2_aligned = y_2
		z_2_aligned = z_2
		vx_1_aligned = vx_1
		vy_1_aligned = vy_1
		vz_1_aligned = vz_1
		vx_2_aligned = vx_2
		vy_2_aligned = vy_2
		vz_2_aligned = vz_2
	endelse
	
	; Rotating around the Y-axis. It allows to compute trajectories not only in th XY plane
	if(~keyword_set(phi)) then begin
		if(keyword_set(verbose)) then print,"Assuming phi = 0. (ie. galaxies have no Z-component)"
		phi=0.
	endif
	x_1_aligned2 = x_1_aligned*cos(phi)
	y_1_aligned2 = y_1_aligned
	z_1_aligned2 = -x_1_aligned*sin(phi)
	x_2_aligned2 = x_2_aligned*cos(phi)
	y_1_aligned2 = y_1_aligned
	z_2_aligned2 = -x_2_aligned*sin(phi)
	vx_1_aligned2 = vx_1_aligned*cos(phi)
	vy_1_aligned2 = vy_1_aligned
	vz_1_aligned2 = -vx_1_aligned*sin(phi)
	vx_2_aligned2 = vx_2_aligned*cos(phi)
	vz_2_aligned2 = -vx_2_aligned*sin(phi)

	
	x_1_aligned = x_1_aligned2
	z_1_aligned = z_1_aligned2 
	x_2_aligned = x_2_aligned2 
	z_2_aligned = z_2_aligned2 
	vx_1_aligned = vx_1_aligned2
	vz_1_aligned = vz_1_aligned2
	vx_2_aligned = vx_2_aligned2
	vz_2_aligned = vz_2_aligned2
	
	; Rotation around Z to have galaxies aligned on a line having an angle theta with the X-axis
	if(~keyword_set(theta)) then begin
		if(keyword_set(verbose)) then print,"Assuming theta = 0. (ie. galaxies have no Y-component)"
		theta_diago = 0.
	endif else theta_diago = theta
	x_1_aligned2 = x_1_aligned*cos(theta_diago)-y_1_aligned*sin(theta_diago)
	y_1_aligned2 = x_1_aligned*sin(theta_diago)+y_1_aligned*cos(theta_diago)
	x_2_aligned2 = x_2_aligned*cos(theta_diago)-y_2_aligned*sin(theta_diago)
	y_2_aligned2 = x_2_aligned*sin(theta_diago)+y_2_aligned*cos(theta_diago)
	vx_1_aligned2 = vx_1_aligned*cos(theta_diago)-vy_1_aligned*sin(theta_diago)
	vy_1_aligned2 = vx_1_aligned*sin(theta_diago)+vy_1_aligned*cos(theta_diago)
	vx_2_aligned2 = vx_2_aligned*cos(theta_diago)-vy_2_aligned*sin(theta_diago)
	vy_2_aligned2 = vx_2_aligned*sin(theta_diago)+vy_2_aligned*cos(theta_diago)
	
	x_1_aligned = x_1_aligned2
	y_1_aligned = y_1_aligned2 
	x_2_aligned = x_2_aligned2 
	y_2_aligned = y_2_aligned2 
	vx_1_aligned = vx_1_aligned2
	vy_1_aligned = vy_1_aligned2
	vx_2_aligned = vx_2_aligned2
	vy_2_aligned = vy_2_aligned2
	
	
	; Computing relative velocity
	v_1_aligned = sqrt(vx_1_aligned^2+vy_1_aligned^2+vz_1_aligned^2)
	v_2_aligned = sqrt(vx_2_aligned^2+vy_2_aligned^2+vz_2_aligned^2)
	v_ini = sqrt((vx_1_aligned-vx_2_aligned)^2+(vy_1_aligned-vy_2_aligned)^2+(vz_1_aligned-vz_2_aligned)^2)
	; Computing orbital velocity of the reduced particle
	if(e lt 1 or e gt 1) then begin
		orbital_speed = sqrt(gamma*((2./rini)-(1./a))/kpc)/(1.0E3)
	endif
	if(e eq 1) then begin
		orbital_speed = sqrt(gamma*(2./rini)/kpc)/1.0E3
	endif
	; Computing orbital energy of the system
	orbital_energy = (0.5*v_ini^2*((mtot1*mtot2)/(mtot1+mtot2))-(G/1.0E9)*unit_mass*(mtot1*mtot2)/((rini)*kpc/1.E3))/1.E4	
	specific_orbital_energy = (0.5*v_ini^2-(G/1.0E9)*unit_mass*(mtot1+mtot2)/((rini)*kpc/1.E3))/1.E4

	recovered_ecc = 1-2.0*per*((1.0/rini)-(kpc*1E-3*1E9/gamma)*(specific_orbital_energy*1.0E4+(G/1.0E9)*unit_mass*(mtot1+mtot2)/((rini)*kpc/1.E3)))

	r_ini = sqrt((x_1_aligned-x_2_aligned)^2+(y_1_aligned-y_2_aligned)^2+(z_1_aligned-z_2_aligned)^2)

	v1_prime=k1*sqrt(gamma)*sqrt((1.0/kpc)*((2.0/rini)-(1.0/a)))*1E-3
	v2_prime=k2*sqrt(gamma)*sqrt((1.0/kpc)*((2.0/rini)-(1.0/a)))*1E-3
	
	
	offsetx = 0.
	offsety = 0.
	offsetz = 0.
	if(keyword_set(center)) then begin
		if(center eq 1) then begin
			offsetx = -x_1_aligned
			offsety = -y_1_aligned
			offsetz = -z_1_aligned
		endif
		if(center eq 2) then begin
			offsetx = -x_2_aligned
			offsety = -y_2_aligned
			offsetz = -z_2_aligned
		endif
		if(center ne 1 and center ne 2) then begin
			print,"CENTER argument should take the value 1 or 2"
			return
		endif
	endif
	x_1_aligned += offsetx 
	y_1_aligned += offsety
	z_1_aligned += offsetz
	x_2_aligned += offsetx
	y_2_aligned += offsety
	z_2_aligned += offsetz
	
	if(~keyword_set(no_x_align)) then begin
		x1 = xt1*cos(theta_gal)-yt1*sin(theta_gal)
		y1 = xt1*sin(theta_gal)+yt1*cos(theta_gal)
	endif else begin
		x1 = xt1
		y1 = yt1
	endelse
	
	xt1 = x1*cos(phi)
	yt1 = y1
	zt1 = -x1*sin(phi)
	
	x1 = xt1*cos(theta_diago)-yt1*sin(theta_diago)
	y1 = xt1*sin(theta_diago)+yt1*cos(theta_diago)
	z1 = zt1
	
	if(~keyword_set(no_x_align)) then begin
		x2 = xt2*cos(theta_gal)-yt2*sin(theta_gal)
		y2 = xt2*sin(theta_gal)+yt2*cos(theta_gal)
	endif else begin
		x2 = xt2
		y2 = yt2
	endelse
	
	xt2 = x2*cos(phi)
	yt2 = y2
	zt2 = -x2*sin(phi)
	
	x2 = xt2*cos(theta_diago)-yt2*sin(theta_diago)
	y2 = xt2*sin(theta_diago)+yt2*cos(theta_diago)
	z2 = zt2
	
	x1 += offsetx
	x2 += offsetx
	y1 += offsety
	y2 += offsety
	z1 += offsetz
	z2 += offsetz
	
	; Output the position/velocity
	pos_xyz = [[x_1_aligned,y_1_aligned,z_1_aligned],[x_2_aligned,y_2_aligned,z_2_aligned]]
	vel_xyz = [[vx_1_aligned,vy_1_aligned,vz_1_aligned],[vx_2_aligned,vy_2_aligned,vz_2_aligned]]
	
	
	
	if(keyword_set(verbose)) then begin
		print,"Mass of the galaxy 1: M1 = "+strtrim(string(mtot1,format='(F8.4)'),1)+" [1E10 solar mass]"
		print,"Mass of the galaxy 2: M2 = "+strtrim(string(mtot2,format='(F8.4)'),1)+" [1E10 solar mass]"
		print,"Center of the galaxy 1: x = "+strtrim(string(x_1_aligned,format='(F8.1)'),1)+" y = "+$
			strtrim(string(y_1_aligned,format='(F8.1)'),1)+" z = "+strtrim(string(z_1_aligned,format='(F8.1)'),1)+" [kpc]"
		print,"Center of the galaxy 2: x = "+strtrim(string(x_2_aligned,format='(F8.1)'),1)+" y = "+$
			strtrim(string(y_2_aligned,format='(F8.1)'),1)+" z = "+strtrim(string(z_2_aligned,format='(F8.1)'),1)+" [kpc]"
		print,"Velocity of the galaxy 1: Vx = "+strtrim(string(vx_1_aligned,format='(F8.1)'),1)+" Vy = "+strtrim(string(vy_1_aligned,format='(F8.1)'),1)+$
			" Vz = "+strtrim(string(vz_1_aligned,format='(F8.1)'),1)+" V = "+strtrim(string(v_1_aligned,format='(F8.1)'),1)+" [km.s^-1]"
		print,"Velocity of the galaxy 2: Vx = "+strtrim(string(vx_2_aligned,format='(F8.1)'),1)+" Vy = "+strtrim(string(vy_2_aligned,format='(F8.1)'),1)+$
			" Vz = "+strtrim(string(vz_2_aligned,format='(F8.1)'),1)+" V = "+strtrim(string(v_2_aligned,format='(F8.1)'),1)+" [km.s^-1]"
		print,"Inital relative velocity: "+string(v_ini,format='(F12.2)')+" [km.s^-1]"
		print,'Orbital velocity of the reduced particle: '+string(orbital_speed,format='(F8.1)')+' [km.s^-1]'
		print,"Specific Orbital energy"+string(specific_orbital_energy,format='(F12.2)')+"  [1E4 km^2.s^-2]"
		print,"Orbital energy"+string(orbital_energy,format='(F12.4)')+" [1E4 kg.km^2.s^-2]"
		print,"Eccentricity"+string(e,format='(F12.4)')
		print,'Initial distance '+strtrim(string(r_ini,format='(F8.1)'),1)+' [kpc]'
		print,"Pericentral distance"+string(per,format='(F12.2)')+" [kpc]"
		if(keyword_set(timesep)) then print,"Pericentral time"+string(t_until_per1,format='(F12.2)')+" [Myrs]"
	endif else if(~keyword_set(silent)) then begin
		openw,lun,output_name,/get_lun
		printf,lun,"Mass of the galaxy 1: M1 = "+strtrim(string(mtot1,format='(F8.4)'),1)+" [1E10 solar mass]"
		printf,lun,"Mass of the galaxy 2: M2 = "+strtrim(string(mtot2,format='(F8.4)'),1)+" [1E10 solar mass]"
		printf,lun,"Center of the galaxy 1: x = "+strtrim(string(x_1_aligned,format='(F8.1)'),1)+" y = "+strtrim(string(y_1_aligned,format='(F8.1)'),1)+" [kpc]"
		printf,lun,"Center of the galaxy 2: x = "+strtrim(string(x_2_aligned,format='(F8.1)'),1)+" y = "+strtrim(string(y_2_aligned,format='(F8.1)'),1)+" [kpc]"
		printf,lun,"Velocity of the galaxy 1: Vx = "+strtrim(string(vx_1_aligned,format='(F8.1)'),1)+" Vy = "+strtrim(string(vy_1_aligned,format='(F8.1)'),1)+$
			" Vz = "+strtrim(string(vz_1_aligned,format='(F8.1)'),1)+" V = "+strtrim(string(v_1_aligned,format='(F8.1)'),1)+" [km.s^-1]"
		printf,lun,"Velocity of the galaxy 2: Vx = "+strtrim(string(vx_2_aligned,format='(F8.1)'),1)+" Vy = "+strtrim(string(vy_2_aligned,format='(F8.1)'),1)+$
			" Vz = "+strtrim(string(vz_2_aligned,format='(F8.1)'),1)+" V = "+strtrim(string(v_2_aligned,format='(F8.1)'),1)+" [km.s^-1]"
		printf,lun,"Inital relative velocity: "+string(v_ini,format='(F12.2)')+" [km.s^-1]"
		printf,lun,"Orbital velocity of the reduced particle: "+string(orbital_speed,format='(F8.1)')+' [km.s^-1]'
		printf,lun,"Specific Orbital energy"+string(specific_orbital_energy,format='(F12.2)')+"  [1E4 km^2.s^-2]"
		printf,lun,"Orbital energy"+string(orbital_energy,format='(F12.4)')+" [1E4 kg.km^2.s^-2]"
		printf,lun,"Eccentricity"+string(e,format='(F12.4)')
		printf,lun,'Initial distance '+strtrim(string(r_ini,format='(F8.1)'),1)+' [kpc]'
		printf,lun,"Pericentral distance"+string(per,format='(F12.2)')+" [kpc]"
		printf,lun,"Pericentral time"+string(t_until_per1,format='(F12.2)')+" [Myrs]"
		close,lun
	endif
		
	if(keyword_set(plot)) then begin
		loadct,39,/silent
		if(~keyword_set(cut1)) then cut1 = 10.*k2
		if(~keyword_set(cut2)) then cut2 = 10.*k1
		points = (2*!PI/99.0) * findgen(100)
		xcircle1 = x_1_aligned + cut1 * cos(points)
		ycircle1 = y_1_aligned + cut1 * sin(points)
		xcircle2 = x_2_aligned + cut2 * cos(points)
		ycircle2 = y_2_aligned + cut2 * sin(points)
		; If no projection plan is given by the user, select the XY plane to prject the trajectory
		if(~keyword_set(xy) and ~keyword_set(xz) and ~keyword_set(yz)) then xy=1
		if(n_elements(thick) eq 0) then thick = 0.9
		if(keyword_set(eps)) then begin
			if(keyword_set(xy)) then set_eps_device,file_name=output_name+'_xy.eps',thick=thick,xsize_cm=18.0,xy_ratio=ratio_xy
			if(keyword_set(xz)) then set_eps_device,file_name=output_name+'_xz.eps',thick=thick,xsize_cm=18.0,xy_ratio=ratio_xy
			if(keyword_set(yz)) then set_eps_device,file_name=output_name+'_yz.eps',thick=thick,xsize_cm=18.0,xy_ratio=ratio_xy

		endif else window,0,xs=512,ys=512
		if(n_elements(vfactor) eq 0) then vfactor = 25./100.
		if(keyword_set(xy)) then begin
			plot,x1,y1,psym=3,xrange=xrange,yrange=yrange,xstyle=1,ystyle=1,$
				xtitle='x [kpc]',ytitle='y [kpc]',/nodata,title=title
			oplot,(x1),(y1)
			oplot,(x2),(y2)
			oplot,[offsetx],[offsety],psym=7,color=150
			;arrow,0,(boxlen/2.)*0.75,100*vfactor,(boxlen/2.)*0.75,/data
			;xyouts,0,(boxlen/2.)*0.80,textoidl('100 km.s^{-1}')
			arrow,x_1_aligned,y_1_aligned,x_1_aligned+vx_1_aligned*vfactor,y_1_aligned+vy_1_aligned*vfactor,/data,color=250,thick=thick*18/3.,hsize=(!D.X_SIZE/64)
			arrow,x_2_aligned,y_2_aligned,x_2_aligned+vx_2_aligned*vfactor,y_2_aligned+vy_2_aligned*vfactor,/data,color=200,thick=thick*18/3.,hsize=(!D.X_SIZE/64)
			oplot,[x_1_aligned],[y_1_aligned],psym=7,color=250
			oplot,[x_2_aligned],[y_2_aligned],psym=7,color=200
			oplot,xcircle1,ycircle1,color=250,psym=-3			
			oplot,xcircle2,ycircle2,color=200,psym=-3
			al_legend,['gal1 [v='+strtrim(string(round(sqrt(vx_1_aligned^2+vy_1_aligned^2))),1)+' km/s]','gal2 [v='+strtrim(string(round(sqrt(vx_2_aligned^2+vy_2_aligned^2))),1)+' km/s]','barycenter'],psym=[7,7,7],color=[250,200,150],box=0
		endif
		if(keyword_set(xz)) then begin
			plot,x1,z1,xrange=xrange,yrange=yrange,xstyle=1,ystyle=1,$
				xtitle='x [kpc]',ytitle='z [kpc]',/nodata,title=title
			oplot,(x1),(z1)
			oplot,(x2),(z2)
			oplot,[offsetx],[offsetz],psym=7,color=150
			;arrow,0,(boxlen/2.)*0.75,100*vfactor,(boxlen/2.)*0.75,/data
			;xyouts,0,(boxlen/2.)*0.80,textoidl('100 km.s^{-1}')
			arrow,x_1_aligned,z_1_aligned,x_1_aligned+vx_1_aligned*vfactor,z_1_aligned+vz_1_aligned*vfactor,/data,color=250,thick=thick*18/3.,hsize=(!D.X_SIZE/64)
			arrow,x_2_aligned,z_2_aligned,x_2_aligned+vx_2_aligned*vfactor,z_2_aligned+vz_2_aligned*vfactor,/data,color=200,thick=thick*18/3.,hsize=(!D.X_SIZE/64)
			oplot,[x_1_aligned],[z_1_aligned],psym=7,color=250
			oplot,[x_2_aligned],[z_2_aligned],psym=7,color=200
			oplot,xcircle1,ycircle1,color=250,psym=-3			
			oplot,xcircle2,ycircle2,color=200,psym=-3
			al_legend,['gal1 [v='+strtrim(string(round(sqrt(vx_1_aligned^2+vz_1_aligned^2))),1)+' km/s]','gal2 [v='+strtrim(string(round(sqrt(vx_2_aligned^2+vz_2_aligned^2))),1)+' km/s]','barycenter'],psym=[7,7,7],color=[250,200,150],box=0
		endif
		if(keyword_set(yz)) then begin
			plot,y1,z1,xrange=xrange,yrange=yrange,xstyle=1,ystyle=1,$
				xtitle='y [kpc]',ytitle='z [kpc]',/nodata,title=title
			oplot,(y1),(z1)
			oplot,(y2),(z2)
			oplot,[offsety],[offsetz],psym=7,color=150
			;arrow,0,(boxlen/2.)*0.75,100*vfactor,(boxlen/2.)*0.75,/data
			;xyouts,0,(boxlen/2.)*0.80,textoidl('100 km.s^{-1}')
			arrow,y_1_aligned,z_1_aligned,y_1_aligned+vy_1_aligned*vfactor,z_1_aligned+vz_1_aligned*vfactor,/data,color=250,thick=thick*18/3.,hsize=(!D.X_SIZE/64)
			arrow,y_2_aligned,z_2_aligned,y_2_aligned+vy_2_aligned*vfactor,z_2_aligned+vz_2_aligned*vfactor,/data,color=200,thick=thick*18/3.,hsize=(!D.X_SIZE/64)
			oplot,[y_1_aligned],[z_1_aligned],psym=7,color=250
			oplot,[y_2_aligned],[z_2_aligned],psym=7,color=200
			oplot,xcircle1,ycircle1,color=250,psym=-3			
			oplot,xcircle2,ycircle2,color=200,psym=-3
			al_legend,['gal1 [v='+strtrim(string(round(sqrt(vy_1_aligned^2+vz_1_aligned^2))),1)+' km/s]','gal2 [v='+strtrim(string(round(sqrt(vy_2_aligned^2+vz_2_aligned^2))),1)+' km/s]','barycenter'],psym=[7,7,7],color=[250,200,150],box=0
		endif
		if(keyword_set(eps)) then begin
			device,/close
			set_plot,'x'
		endif
	endif
end
