## KORBIT ##

"korbit" is a simple IDL routine that allows orbit computation for 2 bodies interaction problem. The routine is designed for galactic interactions, however it can be applied to any 2 body problem.
An extensive description is contained in the header of the routine.